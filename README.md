```bash
# копируем проект
git clone git@gitlab.com:V-eNo-M/kurs.git

# создаём виртуальное окружение
mkvirtualenv --python `which python3.4` -a `pwd` $(basename `pwd`)

# устанавливаем python-модули
pip install -U -r requirements.txt

# создаем БД
./manage.py migrate

# создаем админа
./manage.py createsuperuser

# запуск веб-сервера для разработки
./manage.py runserver
```
