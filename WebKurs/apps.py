from django.apps import AppConfig


class WebkursConfig(AppConfig):
    name = 'WebKurs'
