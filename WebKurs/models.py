from django.db import models

# Create your models here.
# Клиент
class Client(models.Model):
  name = models.CharField(max_length=200)
  phone = models.CharField(max_length=20)
  address = models.CharField(max_length=300)

# Товар
class Good(models.Model):
  name = models.CharField(max_length=200)
  price = models.DecimalField(max_digits=8, decimal_places=2)
  # photo = models.ImageField()

class Order(models.Model):
  date_order = models.DateField()
  client = models.ForeignKey('Client')
  good = models.ForeignKey('Good')
  ready = models.BooleanField()