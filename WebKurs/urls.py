from django.conf.urls import url

from . import views

urlpatterns = [
  url(r'^$', views.default_view, name='home'),
  url(r'^login/$', views.login_view, name='login'),
  url(r'^logout/$', views.logout_view, name='logout'),
  url(r'^registration/$', views.registration_view, name='registration'),

]
