from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponseRedirect


def default_view(request):
    return render(request, 'WebKurs/base.html', context={})


def login_view(request):
    context = {}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        print(username, password)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/')

        elif username != '' and password != '':
            lag = 'Не верный логин или пароль :('
            context = {
                'error': lag,
            }

    return render(request, 'WebKurs/login1.html', context)


def registration_view(request):
    context = {}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        repeat_password = request.POST['repeat_password']
        if password == repeat_password:
            user = User.objects.create_user(username, password=password, email=email)
            user.save()
        else:
            lag = 'Ошибка регистрации :('
            context = {
                'error_reg': lag,
            }
    return render(request, 'WebKurs/login1.html', context)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))
